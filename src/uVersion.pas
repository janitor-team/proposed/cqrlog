unit uVersion;

{$mode objfpc}
interface

const
  cVERSION    = '2.5.1 (001)';
  cMAJOR      = 2;
  cMINOR      = 5;
  cRELEAS     = 1;
  cBUILD      = 1;

  cBUILD_DATE = '2021-01-24';

implementation

end.

